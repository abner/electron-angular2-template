declare var GITLAB_PRIVATE_TOKEN;
declare var GITLAB_HOST;

import {bootstrap as bootAngular} from "angular2/platform/browser";
import {HTTP_PROVIDERS} from 'angular2/http';
import {provide, Component, enableProdMode, PLATFORM_PIPES, ExceptionHandler} from "angular2/core";
import {RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS, LocationStrategy, HashLocationStrategy} from "angular2/router";

let config = require("electronApp.config");

if (config.environment === "production") {
    enableProdMode();
}

import {TranslateService, TranslatePipe, TranslateLoader} from "ng2-translate";
import {CustomTranslateLoader} from "./commonsServices/customTranslateLoader.service";
import {Config} from "./config";
import {CustomExceptionHandler} from "./commonsServices/customExceptionHandler";

import {AppLogger} from "./commonsServices/appLogger";
import {FsLoggerService} from "./commonsServices/fsLogger.service";

import {HomeComponent} from "./components/home/home.component";
import {GitlabConfig} from "./gitlabConfig";
import {GitlabRepositoriesPageComponent} from "./components/gitlabRepositories/gitlabRepositoriesPage.component";
import {GitlabService} from "./components/gitlabRepositories/gitlab.service";
import {GitlabSetupComponent} from "./components/gitlabSetup/gitlabSetup.component";
/**
 * Component Angular App - Componente principal da Aplicação Angular
 */
@Component({
    selector: "my-app",
    template: require("./app.html"),
    providers: [

    ],
    directives: [ROUTER_DIRECTIVES],
    pipes: []

})
/* Definição das Rotas de Navegação */
@RouteConfig([
    /*Página de navegação inicial */
    { path: '/home',       name: "Home",   component: HomeComponent, useAsDefault: true },
    { path: '/gitlab', name: "Gitlab", component: GitlabRepositoriesPageComponent },
    { path: '/gitlab/setup', name: "GitlabSetup", component: GitlabSetupComponent }
])
export class AngularApp {
    constructor(translate: TranslateService) {
        translate.setDefaultLang('pt');
        translate.use('pt');
    }
}

bootAngular(AngularApp, [
    ROUTER_PROVIDERS,
    HTTP_PROVIDERS,
    GitlabService,
    provide(GitlabConfig, {useValue: new GitlabConfig(GITLAB_HOST || "http://gitlab.com", GITLAB_PRIVATE_TOKEN)}),
    provide(TranslateLoader, { useClass: CustomTranslateLoader }),
    provide(AppLogger, { useValue: new FsLoggerService(true)}),
    provide(ExceptionHandler, { useClass: CustomExceptionHandler, deps: [ AppLogger ] }),
    TranslateService,
    provide(PLATFORM_PIPES, { useValue: [TranslatePipe], multi: true }), // aqui, utilizando PLATFORM_PIPES, podem ser colocados 
    // os Pipes que serão disponíveis em todos os componentes
    // provide(LocationStrategy, {useClass: HashLocationStrategy}),
    provide("config", { useClass: Config })
]);

console.log("Aplicação Electron/Angular2 inicializada");    