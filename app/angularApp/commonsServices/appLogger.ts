import {Injectable} from "angular2/core";

@Injectable()
export abstract class AppLogger {
    abstract log(message: string, ...optionalParams: any[]): void;
    abstract  info(message: string, ...optionalParams: any[]): void;
    abstract error(message: string, ...optionalParams: any[]): void;
    abstract warn(message: string, ...optionalParams: any[]): void;
}
