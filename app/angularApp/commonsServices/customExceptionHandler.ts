import {Injectable, Inject, EventEmitter, ExceptionHandler} from "angular2/core";

import {AppLogger} from "./appLogger";
export interface ExceptionEvent {
    error: any;
    stackTrace: any;
    reason: any;
}

class ArrayLogger {
  res = [];
  log(s: any): void { this.res.push(s); }
  logError(s: any): void { this.res.push(s); }
  logGroup(s: any): void { this.res.push(s); }
  logGroupEnd() {
    this.res.forEach(error => {
      console.error(error);
    });
  };
}

/**
 * This class is a custom ExceptionHandler, which
 * logs the exception to a log file and emmits events with mapped/translated messages
 * which allow a visual component to display the error messages to the end user
 */
@Injectable()
export class CustomExceptionHandler extends ExceptionHandler {
    

    public onException: EventEmitter<ExceptionEvent> = new EventEmitter<ExceptionEvent>();
    
    constructor(private appLogger: AppLogger) {
        super(new ArrayLogger(), false);
    }

    call(error, stackTrace = null, reason = null) {
        super.call(error, stackTrace, reason);
        this.onException.emit({ error: error, stackTrace: stackTrace, reason: reason });
        this.appLogger.error(error, reason);
    }
}