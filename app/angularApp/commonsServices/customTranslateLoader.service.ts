import {TranslateLoader} from "ng2-translate";
import {Observable} from "rxjs";


export class CustomTranslateLoader implements TranslateLoader {
    static messages: Object;
    
    static setMessages(messages: Object) {
        CustomTranslateLoader.messages = messages;
    }
    
    getTranslation(lang: string) {
        return Observable.of(CustomTranslateLoader.messages);
    }
}