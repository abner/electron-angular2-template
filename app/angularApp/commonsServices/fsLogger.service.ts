import {Injectable, OnDestroy} from "angular2/core";
import {remote} from "electron";
import {AppLogger} from "./appLogger";

let fs = remote.require("fs");
let path = remote.require("path");

@Injectable()
export class FsLoggerService extends AppLogger implements OnDestroy {

    private _logger: any = require("electron-logger");    
    private file = remote.require("fs");
    
    constructor(private logToConsole: boolean = false) {
        super();
        this._logger.setLevel("info");
        this._logger.setOutput({file: path.join(remote.app.getAppPath(), "logs/app.log")});
        this._logger.open();
    }
    
    ngOnDestroy() {
        this._logger.close();    
    }
    
    log(message: string, ...optionalParams: any[]) {
        if (this.logToConsole) {
            console.log(message, optionalParams);
        }
        this._logger.log(message, optionalParams);
    }
    
    warn(message: string, ...optionalParams: any[]) {
        if (this.logToConsole) {
            console.warn(message, optionalParams);
        }
        this._logger.warn(message, optionalParams);
    }
    
    info(message: string, ...optionalParams: any[]) {
        if (this.logToConsole) {
            console.info(message, optionalParams);
        }
        this._logger.info(message, optionalParams);
    }
    
    error(message: string, ...optionalParams: any[]) {
        if (this.logToConsole) {
            console.error(message, optionalParams);
        }
        this._logger.error(message, optionalParams);
    }
    
    
}