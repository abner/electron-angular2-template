

import {Component, Input} from "angular2/core";

@Component({
    selector: "bs-panel",
    template: require("./bs-panel.html")
})
export class BootstrapPanelComponent {
    @Input() public title: string;
    constructor() {}
}