import 'reflect-metadata';
import {
describe,
expect,
beforeEach,
it,
inject,
injectAsync,
beforeEachProviders
} from 'angular2/testing';
import {provide} from "angular2/core";
import {MockConnection, MockBackend } from "angular2/http/testing";
import {XHRBackend, Http, Response, BaseRequestOptions, ResponseOptions} from "angular2/http";
import {GitlabService} from "./gitlab.service";
import {GitlabConfig} from "../../gitlabConfig";

import {GitlabRepository} from "./interfaces";

describe("GitlabService", () => {

    beforeEachProviders(() => [
        GitlabService,
        provide(GitlabConfig, { useValue: new GitlabConfig("http://gitlab.com", "") }),
        BaseRequestOptions,
        provide(XHRBackend, { useClass: MockBackend }),
        provide(Http,
            {
                useFactory: (backend, options) => {
                    return new Http(backend, options);
                },
                deps: [XHRBackend, BaseRequestOptions]
            })
    ]);

    it("gets a list of GitlabRepositories", inject([XHRBackend, GitlabService], (mockBackend, gitlabService) => {
        let gitlabRepository = { id: 1, name: "Some Project" };
        mockBackend.connections.subscribe(
            (connection: MockConnection) => {
                let resp: ResponseOptions = new ResponseOptions({ status: 200, body: [gitlabRepository] });
                connection.mockRespond(new Response(resp));
            }
        );
        gitlabService.getOwnedRepositories().subscribe((result: GitlabRepository[]) => {
            expect(result.length).toEqual(1);
            expect(result[0]).toEqual(gitlabRepository);
        });
    }));


});