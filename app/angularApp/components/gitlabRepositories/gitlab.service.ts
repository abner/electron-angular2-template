import {Inject, Injectable, OnInit} from "angular2/core";
import {Http, Headers} from "angular2/http";
import {GitlabRequest} from "./gitlabRequest";
import {GitlabConfig} from "../../gitlabConfig";
import {GitlabRepository} from "./interfaces";
import {Observable} from "rxjs";

@Injectable()
export class GitlabService {
    private headers: Headers;
    private ownedRepositoriesRequest: GitlabRequest<GitlabRepository[]> = null;


    constructor( @Inject(Http) private http: Http, private gitlabConfig: GitlabConfig) {
        this.gitlabConfig.configChanged.subscribe((config) => {
            this.gitlabConfig = config;
            this.ownedRepositoriesRequest = null;
        });
    }


    getOwnedRepositoriesRequest(): GitlabRequest<GitlabRepository[]> {
        if (this.ownedRepositoriesRequest === null) {
            let url = this.getUrls()["ownedRepositories"];
            this.ownedRepositoriesRequest = new GitlabRequest<GitlabRepository[]>(this.http, url, this.gitlabConfig);
        }
        return this.ownedRepositoriesRequest;
    }
   
   

    
    private getUrls(): Object {
        return {
            'ownedRepositories': `/api/v3/projects/owned?per_page=${this.gitlabConfig.per_page}`
        };
    }
}