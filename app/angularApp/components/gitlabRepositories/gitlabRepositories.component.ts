import {Component, Input, EventEmitter, Output, Inject, OnInit} from "angular2/core";

import {GitlabService} from "./gitlab.service";
import {GitlabRepository} from "./interfaces";
import {Observable} from "rxjs";
@Component({
    selector: "gitlab-repositories",
    template: require("./gitlabRepositories.html")
})
export class GitlabRepositoriesComponent implements OnInit {
    
    // @Input() repository: string;
    @Output() loadedStarted: EventEmitter<any> = new EventEmitter<any>();
    @Output() repositoriesLoaded: EventEmitter<GitlabRepository[]> = new EventEmitter<GitlabRepository[]>();
    public loading: boolean = false;
    public repositories: GitlabRepository[];

    constructor( @Inject(GitlabService) private gitlabService: GitlabService) {

    }

    loadMore() {
        this.loadedStarted.emit("started");
        this.loading = true;
        this.gitlabService.getOwnedRepositoriesRequest().loadMore().subscribe((repositories) => {
            this.repositories = repositories;
            this.repositoriesLoaded.emit(repositories);
            this.loading = false;
        });
    }

    ngOnInit() {
        this.loading = true;
        this.gitlabService.getOwnedRepositoriesRequest().execute().subscribe((repositories) => {
            this.repositories = repositories;
            this.repositoriesLoaded.emit(repositories);
            this.loading = false;
        });
                   
        // });
    }
}