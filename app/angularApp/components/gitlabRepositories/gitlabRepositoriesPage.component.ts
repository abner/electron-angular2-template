import {Component, OnInit} from "angular2/core";
import {ROUTER_PROVIDERS, ROUTER_DIRECTIVES} from "angular2/router";
import {GitlabRepository} from "./interfaces";
import {GitlabRepositoriesComponent} from "./gitlabRepositories.component";

@Component({
        selector: 'gitlab-page',
        template: require("./gitlabRepositoriesPage.html"),
        providers: [ROUTER_PROVIDERS],
        directives: [ROUTER_DIRECTIVES, GitlabRepositoriesComponent],
        host: {'class' : 'ng-animate gitlabRepositoriesPageContainer'},
        styles: [require("./gitlabRepositoriesPage.scss").toString()]
})
export class GitlabRepositoriesPageComponent implements OnInit {
    loading: boolean = false;
    constructor() {
        
    }
    
    ngOnInit() {
        this.loading = true;
    }
    
    startingLoad() {
        this.loading = true;
    }
    
    afterRepositoriesLoaded(repositories: GitlabRepository[] ) {
        console.log("Repositories loaded...", repositories);
        this.loading = false;
    }
}