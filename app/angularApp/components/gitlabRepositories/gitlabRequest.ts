import {Injectable} from "angular2/core";
import {Http, Headers, Response} from "angular2/http";
import {Observable} from "rxjs";
import {GitlabRepository} from "./interfaces";
import {GitlabConfig} from "../../gitlabConfig";
export class GitlabRequest<T> {

    result: T[] = null;

    private nextUrl: string;
    private lastUrl: string;
    private per_page: number;
    private requestHeaders: Headers = new Headers();

    constructor(private http: Http, private initialUrl: string, private gitlabConfig: GitlabConfig) {
        this.fillHeaders();
    }
    
    execute(): Observable<T[]> {
        if (this.result == null) {
            let request = this.http.get(`${this.gitlabConfig.host}${this.initialUrl}`, { headers: this.requestHeaders });
            return this.processRequest(request);
        } else {
            return Observable.of(this.result);
        }
    }
    
    private processRequest(response: Observable<Response>): Observable<T[]> {
        let responseMappedObs = response.map(this.mapResponseProcessor());
         
        return responseMappedObs.map(this.mappedResponseProcessor());
     }

    private mappedResponseProcessor() {
        return ((data: any) => {
            this.result = this.result === null ? data : this.result.concat(data);
            return this.result;
        }).bind(this);
    }

    private mapResponseProcessor() {
        return ((response: Response) => {
            this.extractHeaders(response.headers);
            return response.json();
        }).bind(this);
    }

    loadMore(): Observable<T[]> {
        let request = this.http.get(this.nextUrl, { headers: this.requestHeaders });
        return this.processRequest(request);
    }

    private fillHeaders() {
        if (!this.requestHeaders.has("ACCEPT")) {
            this.requestHeaders.append("ACCEPT", "application/json");
        }
        if (!this.requestHeaders.has("PRIVATE-TOKEN")) {
            this.requestHeaders.append("PRIVATE-TOKEN", this.gitlabConfig.apiToken);
        }
    }

    private extractHeaders(headers: Headers) {
        if (headers.has("Link")) {
            let headerLinks = this.parseLinkHeader(headers.get("Link"));
            this.nextUrl = headerLinks.next;
            this.lastUrl = headerLinks.last;
            
        } else {
            this.lastUrl = null;
            this.nextUrl = null;
        }
    }

    private parseLinkHeader(header: string): {last: string; next: string} {
        if (header.length === 0) {
            throw new Error("input must not be of zero length");
        }

        // Split parts by comma
        let parts = header.split(',');
        let links = {};
        // Parse each part into a named link
        for (let i = 0; i < parts.length; i++) {
            let section = parts[i].split(';');
            if (section.length !== 2) {
                throw new Error("section could not be split on ';'");
            }
            let url = section[0].replace(/<(.*)>/, '$1').trim();
            let name = section[1].replace(/rel="(.*)"/, '$1').trim();
            links[name] = url;
        }
        return <any>links;
    }

    
}