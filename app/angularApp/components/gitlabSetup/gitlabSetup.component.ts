import {Component} from "angular2/core";
import {GitlabConfig} from "../../gitlabConfig";
import {BootstrapPanelComponent} from "./../commons/bootstrapPanel.component";
@Component({
    selector: "gitlab-setup",
    template: require("./gitlabSetup.html"),
    directives: [BootstrapPanelComponent]
})
export class GitlabSetupComponent {
    constructor(public gitlabConfig: GitlabConfig) {
        
    }
}