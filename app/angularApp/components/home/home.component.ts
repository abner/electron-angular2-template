import {Component, Input }       from "angular2/core";
import {ROUTER_PROVIDERS, ROUTER_DIRECTIVES} from "angular2/router";
@Component({
    selector: 'home',
    template: require("./home.html"),
    providers: [ROUTER_PROVIDERS],
    directives: [ROUTER_DIRECTIVES],
    host: {'class' : 'ng-animate homeContainer'},
    styles: [require("./home.scss").toString()]
})
export class HomeComponent {
    public title: string = "Bem vindo à aplicação Electron / Angular 2";
    
    constructor() {
        
    }
}