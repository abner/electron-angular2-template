import {provide, Component} from "angular2/core";
import * as path from "path";

let packageJSON = require("../../package.json");
const BASE_PATH = require("remote").app.getAppPath();

interface AppConfig {
    version: string;
    appName: string;
    basePath: string;
}

export class Config {
    app: AppConfig;
    constructor() {
        this.app = {
            version: packageJSON.version,
            appName: packageJSON.name,
            basePath: BASE_PATH
        };
    }
}