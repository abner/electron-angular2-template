import {EventEmitter} from "angular2/core";

export class GitlabConfig {
    configChanged: EventEmitter<GitlabConfig> = new EventEmitter<GitlabConfig>();
    private _host: string;
    private _apiToken: string;

    get host(): string {
        return this._host;
    }

    set host(value: string) {
        if (value === undefined) throw 'Please supply host';
        this._host = value;
        this.configChanged.emit(this);
    }
    
    get apiToken(): string {
        return this._apiToken;
    }

    set apiToken(value: string) {
        if (value === undefined) throw 'Please supply apiToken';
        this._apiToken = value;
        this.configChanged.emit(this);
    }

    constructor(host: string, apiToken: string) {
        this.host = host;
        this.apiToken = apiToken;
    }

    public per_page: number = 10;
}