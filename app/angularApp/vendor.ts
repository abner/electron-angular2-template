// import 'es6-shim';
// import 'angular2/bundles/angular2-polyfills';
// Polyfills
// These modules are what's in angular 2 bundle polyfills so don't include them
// 
// import 'es6-promise';
import 'reflect-metadata';

// CoreJS has all the polyfills you need (???)
import 'core-js';

import 'zone.js/dist/zone';
import 'zone.js/dist/long-stack-trace-zone';

// Bootstrap
(<any>global).jQuery = (<any>window).jQuery = (<any>window).$ = require('jquery');
import 'bootstrap-sass/assets/javascripts/bootstrap.js';

// Angular 2 Imports
import 'angular2/platform/browser';
import 'angular2/core';
import 'angular2/http';
import 'angular2/router';

// Outros Modulos
// import 'moment';
// import 'ag-grid';
// import 'ag-grid-ng2';
// import 'fluentreports';
// import 'lodash';