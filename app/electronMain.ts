// importando o objeto namespace electron
import * as electron from "electron";

import {BrowserWindow, app} from "electron";

import electronReload from "./electronReload";

let path = require("path");
let packageJson = require("../package.json");

// Interface descrevendo os argumentos que este script conhece
interface AppArgV {
    test: boolean;
    dev: boolean;
    report: boolean;
    debug: boolean;
    ci: boolean;
}


// Faz o parse dos argumentos e o transforma em um objeto
let argv: AppArgV = require("yargs").parse(process.argv);

// console.log("ARGUMENTOS_", argv);

// Se for mode desenvolvimento então ativa o module eletron-reload
// para fazer reload da aplicação caso haja mudanças nos htmls ou javascripts
if (argv.dev) {
    console.log("DEV MODE - LIVE RELOAD ATIVO !!! ");
    /* Para recarregar o electron quando os arquivos são alterados */
    electronReload(path.join(path.resolve(__dirname), "assets"), {
        "electron": require("electron-prebuilt"),
        "electronArgs": "--dev"
    });
}


// Manter uma referencia global ao objeto window, se não o fizer, a janela
// será fechada automaticamente quando o objeto for coletado pelo
// garbage collector do javascript
let mainWindow, testWindow: Electron.BrowserWindow = null;


// Fechar a aplicação electron quando todas as janelas forem fechadas
app.on("window-all-closed", () => {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== "darwin") {
        app.quit();
    }
});

if (argv.test) {
    electronReload(path.join(path.resolve(__dirname), "assets"), {
        "electron": require("electron-prebuilt"),
        "electronArgs": "--test"
    });

  
    app.on("ready", () => {
        testWindow = new BrowserWindow({ width: 1024, height: 600, show: !argv.ci });

        if (!argv.ci) testWindow.maximize();
        
        // carregando o html da aplicação
        
        let htmlFile = path.join(app.getAppPath(), "app", "test.html");
        testWindow.loadURL("file://" + htmlFile);
        

        // Abrindo o DevTools.
        // mainWindow.webContents.openDevTools();

        // Evento emitido quando a janela é fechada
        testWindow.on("closed", function() {
            for (let window of electron.BrowserWindow.getAllWindows()) {
                window.destroy();
            }           
            // Liberando o objeto window
            // se sua aplicação for multijanela, então você deverá ter um 
            // array com as janelas que foram abertas, neste caso essa seria
            // a hora de excluir o obeto janela deste array
            testWindow = null;
        });
    });

}
else if (argv.report) {
    app.on("ready", () => {
        // Criando o objeto BrowserWindow
        mainWindow = new BrowserWindow({ width: 1024, height: 600, webPreferences: { nodeIntegration: false } });
        mainWindow.maximize();
        // carregando o html da aplicação
        let htmlFile = path.join(app.getAppPath(), "/e2e/allure-reports/index.html");
        mainWindow.loadURL("file://" + htmlFile);
    });
}
else {

    // O evento ready do objeto electron.app é disparado quando o
    // electron está pronto e então será possível a criação de janelas de navegador
    app.on("ready", () => {
        // Criando o objeto BrowserWindow
        mainWindow = new BrowserWindow({ width: 1024, height: 600, minWidth: 1024, minHeight: 600 });

        mainWindow.maximize();

        let htmlFile = path.join(app.getAppPath(), "app/index.html");
        // carregando o html da aplicação
        mainWindow.loadURL("file://" + htmlFile);

        // Abrindo o DevTools.
        if (argv.debug && typeof argv.debug === 'boolean') {
            mainWindow.webContents.openDevTools();
        }
        

        // Evento emitido quando a janela é fechada
        mainWindow.on("closed", function() {
            for (let window of electron.BrowserWindow.getAllWindows()) {
                window.destroy();
            } 
            // Liberando o objeto window
            // se sua aplicação for multijanela, então você deverá ter um 
            // array com as janelas que foram abertas, neste caso essa seria
            // a hora de excluir o obeto janela deste array
            mainWindow = null;
        });
    });
}
