let app = require("electron").app;

let chokidar = require("chokidar");
let extend = require("util")._extend;

import * as fs from "fs";

export default function(glob: string, options: any) {

    options = options || {};
    let path = require("path");
    let browserWindows: Electron.BrowserWindow[] = [];
    let opts = extend({ ignored: /node_modules|[\/\\]\./ }, options);
    let watcher = chokidar.watch(glob, opts);

    let onChange = function() {
        browserWindows.forEach(function(bw) {
            let htmlFile = path.join(app.getAppPath(), "app", "index.html");
            if (options.electronArgs === "--test") {
                htmlFile = path.join(app.getAppPath(), "app", "test.html");
            }

            // bw.webContents.reloadIgnoringCache();

            bw.loadURL("file://" + htmlFile);
        });
    };

    app.on("browser-window-created", function(e, bw) {
        browserWindows.push(bw);
        let i = browserWindows.indexOf(bw);

        bw.on("closed", function() {
            browserWindows.splice(i, 1);
        });
    });

    // Preparing hard reset
    let eXecutable = options.electron;
    let args = options.electronArgs;

    if (eXecutable && fs.existsSync(eXecutable)) {
        let proc = require("child_process");


        let appPath = app.getAppPath();
        // 
        //         let config: any = require(path.join(appPath, "package.json"));
        let mainFile = path.join(appPath, "app/assets/electronApp.js");

        chokidar.watch(mainFile).on("change", function() {
            proc.spawn(eXecutable, [appPath, args]);
            // Kamikaze!
            app.quit();
        });
    } else {
        console.log("Electron could not be found. No hard resets for you!");
    }

    watcher.on("change", onChange);
};

