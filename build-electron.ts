/**
 * Script para a construção da aplicação Electron/Angular2
 */
import * as path from "path";
import * as yargs from "yargs";
import * as glob from "glob";
import * as fs from "fs";
import * as shelljs from "shelljs";




let project_path = path.join(__dirname, "./");
let app_path = path.join(__dirname, "./app");
let tmp_path = path.join(__dirname, "./.tmp");
let tmp_app_path = path.join(__dirname, "./.tmp/app");


function getElectronPackagerConfig(platformArg, archArg) {
    return {
        dir: tmp_path,
        name: "Electron-Angular2",
        platform: platformArg,
        arch: archArg,
        version: '0.36.7',
        out: `${path.join(project_path, "releases")}`,
        'app-bundle-id': 'electron-angular2',
        'app-version': '0.0.1',
        overwrite: true,
        asar: false
    };
}
    

// passo 1 - leitura dos parâmetros de script (argumentos do comando)
let argv = yargs.usage('Forma de uso $0 <command> [options]')
    .example('$0 -t linux-32', 'Constrói a versão linux 32 bits')
    .demand("t")
    .alias("t", "target")
    .nargs("t", 1)
    .describe("t", "A plataforma alvo")
    .help("h")
    .alias("h", "help").argv;



// passo 3 - copia arquivos js
//           na pasta  ./tmp/app/
console.log(`Iniciando a build do app Electron/Angular2 para a plataforma ${argv.target} ...`);


function excluirPastaTmp() {
    // passo 0.1 - remover a pasta .tmp
    try {
        shelljs.rm("-Rf", tmp_path);
    } catch (e) {
        console.error("Erro ao excluir a pasta tmp", e);
        process.exit(1);
    }

    console.log("Pasta .tmp apagada");
}

function criarPastaTmp() {
    console.log("Criando a pasta .tmp ...");
    // passo 0.1 - criar a pasta .tmp
    try {
        shelljs.mkdir(tmp_path);
    } catch (e) {
        console.error("Erro ao criar a pasta tmp", e);
        process.exit(1);
    }
    console.log("Pasta .tmp criada.");
}

function compilacaoWebpack() {
    console.log("Iniciando a compilação webpack ...");
    let webpackResult = shelljs.exec("webpack --production");
    if (webpackResult.code !== 0) {
        console.error("Erro na compilação Webpack", webpackResult.output);
        process.exit(1);
    }
    console.log("Finalizado a compilação webpack.");
}

function criarPackageJSON() {
    console.log("Criando o package.json na pasta .tmp");
    // cria o package.json (requerido pelo electron - na pasta .tmp)
    let projectPackageJSON = require(path.join(project_path, "package.json"));

    let appPackageJSON = projectPackageJSON;

    delete appPackageJSON["devDependencies"];
    delete appPackageJSON["scripts"];

    console.log("Criando o package.json sem a informação de dependências na pasta .tmp");

    try {
        fs.writeFileSync(path.join(tmp_path, "package.json"), JSON.stringify(appPackageJSON, null, "\t"));
    } catch (e) {
        console.error(e);
        throw new Error("Erro ao criar arquivo package.json na pasta .tmp");
    }
}

function copiarAssets() {
    console.log("Copiando os arquivos da aplicação para a pasta .tmp/app ...");

    let assetsFiles = glob.sync("app/**/*.+(css|html|png|jpg|gif|map|eot|svg|ttf|woff|woff2)", {
        ignore: [
            "**/*.spec.js",
            "index.html",
            "index.bkp.html",
            "teste.html"
        ],
        cwd: project_path
    });

    // ["resources", "db"].forEach((pasta) => {
    //     shelljs.cp("-R", path.join(project_path, pasta), tmp_path);
    // });

    function targetPathForFile(f) {
        return path.join(tmp_path, path.relative(project_path, f));
    }

    let assetsFilesMap = assetsFiles.map((f) => {
        return { source: path.join(project_path, f), target: targetPathForFile(f) };
    });


    assetsFilesMap.forEach((fileMap) => {
        let targetPath = path.dirname(fileMap.target);
        if (!fs.existsSync(targetPath)) {
            shelljs.mkdir("-p", targetPath);
        }
        shelljs.cp(fileMap.source, fileMap.target);
    });
    console.log("Fim da cópia dos arquivos");
}

function copiarIndexHtml() {
    console.log("Copiando index.html ...");
    shelljs.cp(path.join(app_path, "index.html"), path.join(tmp_app_path, "index.html"));
    console.log("Fim da cópia do index.html.");
}


let modulesCachePath = path.join(project_path, ".modules");
let modulesCachePathOnTmp = path.join(project_path, ".tmp", ".modules");
function updateNodeModules() {
    shelljs.rm("-Rf", modulesCachePath);
    shelljs.exec("pac -P");
    shelljs.mv(modulesCachePath, modulesCachePathOnTmp);
    shelljs.exec(`cd ${tmp_path} && ../node_modules/.bin/pac -P install && npm rebuild`);
    shelljs.rm("-Rf", modulesCachePathOnTmp);
}

function execElectronBuild() {
    // passo 5 - chamar o epi (Electron Packager Interactive para construir o pacote da aplicação)
    console.log("Iniciando a construção da aplicação Electron");

    let packager = require('electron-packager');
    let platformArg = argv.target.split("-")[0];
    let archArg = argv.target.split("-")[1];
    packager(getElectronPackagerConfig(platformArg, archArg), (err, appPaths) => {
        if (err) {
            console.error(err);
            throw new Error("Erro ao criar pacote da aplicação Electron-Angular2!");
        }
        console.info(`Pacote da aplicação Electron-Angular2 criado com sucesso! Plataforma: ${platformArg}/${archArg}`);
    });
    
    //    console.warn("Pendente a implementação da chamada do EPI");
}


if (!argv.skipWebpack) {
    excluirPastaTmp();
    criarPastaTmp();
    compilacaoWebpack();
}

criarPackageJSON();
copiarAssets();
copiarIndexHtml();
// updateNodeModules();
execElectronBuild();

