import * as webpack from "webpack";
import * as path from "path";

import * as webpackAngular from "./webpack-angular.config";
import * as webpackElectron from "./webpack-electron.config";
import * as webpackTests from "./webpack-tests.config";

import {argv} from "./webpack-args";

export interface AllWebpackConfigs {
    webpackAngularConfig: webpack.Configuration;
    webpackElectronConfig: webpack.Configuration;
    webpackTestsConfig: webpack.Configuration;
}

let uglify: boolean = false;


let outputPath = path.join(__dirname, "..", "app/assets");

if (argv.production) {   
    outputPath = path.join(__dirname, "..", ".tmp", "app/assets");
}

// exporta os três Configs Webpack (Angular, Electron e Tests)
export var AllWebpackConfigs: AllWebpackConfigs = {
    webpackAngularConfig: webpackAngular.config(outputPath),
    webpackElectronConfig: webpackElectron.config(outputPath),
    webpackTestsConfig: webpackTests.config(outputPath)
};
