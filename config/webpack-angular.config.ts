import * as webpack from 'webpack';
import * as path from "path";
import * as child_process from "child_process";
import {argv} from "./webpack-args";
import * as _ from "lodash";

let ExtractTextPlugin = require("extract-text-webpack-plugin");
let electronProcess = null;

let basicProvidedObject = {
    $: "jquery",
    jQuery: "jquery",
    "window.jQuery": "jquery"
};


// Configuração webpack dos bundles da aplicação Angular a ser carregada
// no mainBrowserWindow da aplicação Electron
export function config(outputPath: string, providedObject: {} = {}): webpack.Configuration {
    let webpackProvided = _.extend(basicProvidedObject, providedObject);
    let configObj = <any>{
        entry: {
            'angularVendor': './app/angularApp/vendor.ts',
            'angularApp': './app/angularApp/app.ts',
            'bootstrap-bundle': 'bootstrap-loader'
        },

        devtool: "source-map",

        output: {
            path: outputPath,
            libraryTarget: 'commonjs2',
            publicPath: '/',
            filename: '[name].js',
            chunkFilename: '[name].js'
        },

        target: "electron-renderer",

        plugins: [
            new webpack.optimize.CommonsChunkPlugin("angularCommons.js", ["angularVendor", "angularApp"]),
            new ExtractTextPlugin("styles.css"),
            new webpack.ProvidePlugin(webpackProvided),
            new webpack.DefinePlugin({
                GITLAB_PRIVATE_TOKEN: JSON.stringify(process.env.GITLAB_PRIVATE_TOKEN),
                GITLAB_HOST: JSON.stringify(process.env.GITLAB_HOST)
            })
        ],


        resolve: {
            extensions: ['', '.webpack.js', '.web.js', '.ts', '.js', 'scss', '.html'],
            alias: {
                "electronApp.config": path.join(__dirname, '../app/runtimeConfig') + `/${process.env.NODE_ENV}.json`
            }
        },

        module: {

            preLoaders: [
                {
                    test: /\.js$/,
                    loader: 'source-map-loader',
                    exclude: [
                        // esses dois pacotes apresentam problemas com os seus source-maps
                        path.join(__dirname, '..', 'node_modules', 'rxjs'),
                        path.join(__dirname, '..', 'node_modules', '@angular2-material'),
                    ]
                }
            ],
            noParse: [
                // esses dois pacotes não são commons-js e portanto evitamos que o webpack
                // faça o parse deles
                path.join(__dirname, '..', 'node_modules', 'zone.js', 'dist'),
                path.join(__dirname, '..', 'node_modules', 'angular2', 'bundles')
            ],
            loaders: [
                // configurando os loaders (Typescript, JSON, css e dos arquivos de imagens e fontes)
                { test: /\.ts$/, loader: "ts-loader" },
                { test: /\.scss$/, loaders: ['raw-loader', 'sass-loader?sourceMap'] },
                { test: /\.html$/, loader: "raw-loader" },
                { test: /\.json$/, loader: "json-loader" },
                { test: /bootstrap-sass[\/\\]assets[\/\\]javascripts[\/\\]/, loader: 'imports?jQuery=jquery' },
                { test: /\.css$/, loader: ExtractTextPlugin.extract("style-loader", "css-loader") },
                { test: /.(png|woff(2)?|eot|ttf|svg)(\?[a-z0-9=\.]+)?$/, loader: 'url-loader?limit=200000' }
            ]
        }


    };

    // se chamado o webpack com o argumento --dev, então após a finalização da primeira compilação
    // a aplicação electron será inicializada com o  argumento --dev
    // a aplicação já possui um algoritmo que checa os arquivos na pasta app/assets e recarrega a página
    // após modificações
    if (argv.dev) {
        let WebpackOnBuildPlugin = require('on-build-webpack');
    
        // configure the webPackOnBuildPlugin with our post-compilation function as argument
        let onBuildPluginConfig = new WebpackOnBuildPlugin(function(stats) {

            function spawnElectronChildProcess() {
                if (!electronProcess) {
                    let electronArgs = [path.join(__dirname, ".."), "--dev"];

                    if (argv.debug && typeof argv.debug !== 'boolean') {
                        electronArgs.push("--debug");
                    }
                    else {
                        electronArgs.push("--debug=" + argv.debug);
                    }

                    electronProcess = child_process.spawn("electron", electronArgs, {
                        stdio: 'inherit'
                    });
                    electronProcess.on('exit', function() {
                        electronProcess = null;
                    });
                }
            }
            setTimeout(spawnElectronChildProcess, 1000);
        });

        configObj.plugins.push(onBuildPluginConfig);
    }

    return configObj;
};