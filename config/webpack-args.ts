import * as yargs from "yargs";

interface WebpackArgv extends yargs.Argv {
    dev: boolean;
    production: boolean;
    debug: boolean | number;
    test: boolean;
    spec: string;
    ci: boolean;
}

// usa o módulo node yargs para ler os argumentos passados ao processo WebpackArgv
// e os exporta na variável argv 
export var  argv: WebpackArgv = yargs.argv;