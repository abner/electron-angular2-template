import * as webpack from 'webpack';

// Configuração webpack dos bundles da aplicação Electron a ser carregada
// pelo Electron da aplicação PerDCOMP
export function config(outputPath: string): webpack.Configuration {
    return <webpack.Configuration>{
        entry: {
            'electronApp': './app/electronMain.ts'
        },

        output: {
            path: outputPath,
            libraryTarget: 'commonjs2',
            publicPath: '/',
            filename: '[name].js',
            chunkFilename: '[name].js'
        },

        // externals: nodeModules,

        node: {
            __filename: true,
            __dirname: true
        },

        target: "electron-main",

        devtool: "sourcemap",

        plugins: [

        ],

        resolve: {
            extensions: ['', '.webpack.js', '.web.js', '.ts', '.js']
        },

        module: {
            loaders: [
                { test: /\.ts$/, exclude: /\.spec\.ts$/, loader: "ts-loader" },
                { test: /\.json$/, loader: "json-loader" }
            ]
        }
    };
};