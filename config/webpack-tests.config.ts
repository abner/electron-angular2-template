/**
 *  Webpack config to build the test-bundle file
 */

import * as glob from "glob";
import * as webpack from "webpack";
import * as path from "path";
import {argv} from "./webpack-args";
import * as child_process from "child_process";

let ExtractTextPlugin = require("extract-text-webpack-plugin");

// ler o path de todos os specs na pasta path para 
// que seja gerado um bundle com esses arquivos 
let testsFiles = glob.sync("./app/**/*.spec.ts");

// se um spec específico for passado, então gera um bundle só com esse spec
if (argv.spec) {
    
    let specName = argv.spec;
    let specBaseName = path.basename(argv.spec, ".ts");
    if (!specBaseName.endsWith("spec")) {
       specName = specName.replace(".ts", ".spec.ts"); 
    }
    
    let specificSpecFiles = glob.sync(`./app/**/${specName}`);
    
    if (specificSpecFiles.length > 0) {
        testsFiles = specificSpecFiles;
    }
    
    testsFiles.push(path.join(__dirname, "../app/angularApp/vendor.ts"));
}

let testProcess = null;

// Configuração webpack do bundle de teste da aplicação Electron-Angular2
export function config(outputPath: string): webpack.Configuration {
    let config = <any>{
        entry: testsFiles,
        devtool: "sourcemap",
        output: {
            libraryTarget: "commonjs2",
            path: outputPath,
            filename: "tests.js"
        },
        resolve: {
            extensions: ['', '.webpack.js', '.web.js', '.ts', '.js']
        },

        target: "electron-renderer",
        
        plugins: [],

        module: {

            preLoaders: [
                {
                    test: /\.js$/,
                    loader: 'source-map-loader',
                    exclude: [
                        // these packages have problems with their sourcemaps
                        path.join(__dirname, '..', 'node_modules', 'rxjs'),
                        path.join(__dirname, '..', 'node_modules', '@angular2-material'),
                    ]
                }
            ],
            noParse: [
                path.join(__dirname, '..', 'node_modules', 'zone.js', 'dist'),
                path.join(__dirname, '..', 'node_modules', 'angular2', 'bundles')
            ],

            loaders: [
                { test: /\.ts$/, loader: "ts-loader" },
                { test: /\.json$/, loader: "json-loader" },
                { test: /.(png|woff(2)?|eot|ttf|svg)(\?[a-z0-9=\.]+)?$/, loader: 'url-loader?limit=100000' },
                { test: /\.css$/, loader: ExtractTextPlugin.extract("style-loader", "css-loader") }
            ]
        }

    };

    let WebpackOnBuildPlugin = require('on-build-webpack');
    
    // configure the webPackOnBuildPlugin with our post-compilation function as argument
    let onBuildPluginConfig = new WebpackOnBuildPlugin(function(stats) {
        function spawnChildProcessTest() {
            if (!testProcess) {
                testProcess = child_process.spawn("electron", [".", "--test"], {
                    stdio: 'inherit'
                });
                testProcess.on('exit', function() {
                    testProcess = null;
                });
            }
        }
        setTimeout(spawnChildProcessTest, 1100);
    });
    
    config.plugins.push(onBuildPluginConfig);

    return config;
}