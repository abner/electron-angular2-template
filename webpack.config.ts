import * as path from "path";
import * as yargs from "yargs";

import {argv} from "./config/webpack-args";

import * as webpack from 'webpack';

if (argv.production) {
    process.env.NODE_ENV = "production";
} else if (argv.test) {
    process.env.NODE_ENV = "test";
} else {
    process.env.NODE_ENV = "development";
}

import {AllWebpackConfigs} from "./config/webpack-all";

let applicationConfigTargets = [AllWebpackConfigs.webpackElectronConfig, AllWebpackConfigs.webpackAngularConfig];
let testConfigTargets = [AllWebpackConfigs.webpackElectronConfig, AllWebpackConfigs.webpackTestsConfig];
 
if (argv.production) {
    applicationConfigTargets.forEach((config: webpack.Configuration) => {
        config.plugins.push(new webpack.optimize.UglifyJsPlugin({
            warnings: false,
            mangle: false/*{
                except: ['$super', '$', 'exports', 'require', 'module']
            }*/
        }));
    });
}

if (argv.test) {
    module.exports = testConfigTargets;
} else {
    module.exports = applicationConfigTargets;
}
